<?php
/**
 * @file
 */


/**
 * Class entity_boxes.
 */
class entity_boxes extends boxes_box {

  /**
   * Implementation of boxes_box::options_defaults().
   */
  public function options_defaults() {
    return array(
      'field' => '',
      'label' => 'above'
    );
  }

  /**
   * Implementation of boxes_box::options_form().
   */
  public function options_form(&$form_state) {
    $form = array();

    $options = array();

    $instances = field_info_instances();
    foreach ($instances as $type => $entity) {
      foreach ($entity as $bundle => $fields) {
        foreach ($fields as $field => $info) {
          $options[$bundle][$type . ':' . $bundle . ':' . $field] = $field;
        }
      }
    }

    $form['field'] = array(
      '#title' => 'Entity field',
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->options['field']
    );
    $form['label'] = array(
      '#title' => t('Label position'),
      '#type' => 'select',
      '#options' => array(
        'inline' => 'inline',
        'above' => 'above',
        'hidden' => 'hidden',
      ),
      '#default_value' => $this->options['label']
    );

    return $form;
  }

  /**
   * Implementation of boxes_box::render().
   */
  public function render() {
    $entities = _entity_boxes_get_entity();

    $content = '';

    list($entity_type, $bundle, $field) = explode(':', $this->options['field'], 3);

    if (isset($entities[$entity_type])) {
      foreach ($entities[$entity_type] as $entity) {
        if ($entity['entity']->type === $bundle) {
          $entity_field = field_view_field(
            $entity_type,
            $entity['entity'],
            $field,
            array(
              'label' => $this->options['label']
            ),
            $entity['meta']['langcode']
          );

          $content .= render($entity_field);
        }
      }
    }

    return array(
      'delta' => $this->delta,
      'title' => $this->title,
      'subject' => '',
      'content' => $content,
    );
  }

}
